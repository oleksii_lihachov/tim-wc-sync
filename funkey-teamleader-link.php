<?php
/**
 * Plugin Name: Funkey - Teamleader link
 * Description: Link between Teamleader & Funkey.
 * Version: 1.0
 * Author: Igor Gai
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
global $teamleader_requests_sender;

define( 'FT_VERSION', '1.0' );
define( 'FT_TEXT_DOMAIN', 'funkey-teamleader' );
define( 'FT_PLUGIN_SLUG', 'funkey-teamleader' );
define( 'FT_PLUGIN_DIR', __DIR__ );

include __DIR__ .'/vendor/autoload.php';

require_once __DIR__ . '/classes/FunkeyTeamleaderAdminPage.php';
require_once __DIR__ . '/classes/FunkeyTeamleaderClient.php';
require_once __DIR__ . '/classes/FunkeyTeamleaderRequestsSender.php';
require_once __DIR__ . '/classes/FunkeyTeamleaderWooSettings.php';
require_once __DIR__ . '/classes/FunkeyTeamleaderOrdersActions.php';
require_once __DIR__ . '/classes/FunkeyTeamleaderHooks.php';
require_once __DIR__ . '/includes/Main.php';

register_activation_hook( __FILE__, 'ftml_activation_function' );

// Create rows in wp_options table
function ftml_activation_function() {
    add_option('ftm_client_id', '', '', false);
    add_option('ftm_client_secret', '', '', false);
    add_option('ftm_access_token', [], '', false);
    add_option('ftm_price_quote_prefix', '', '', false);
    add_option('ftm_deal_phase_sent', '', '', false);
    add_option('ftm_deal_phase_confirmed', '', '', false);
    add_option('ftm_activity_name_field_id', '', '', false);
    add_option('ftm_order_url_field_id', '', '', false);
    add_option('ftm_catering_field_id', '', '', false);
    add_option('ftm_order_id_field_id', '', '', false);
    add_option('ftm_attachment_field_id', '', '', false);
    add_option('ftm_number_participants_field_id', '', '', false);
    add_option('ftm_activity_duration_field_id', '', '', false);
    add_option('ftm_activity_location_field_id', '', '', false);
    add_option('ftm_activity_date_field_id', '', '', false);
    add_option('ftm_activity_time_field_id', '', '', false);
    add_option('ftm_request_type_field_id', '', '', false);
    add_option('ftm_discount_field_id', '', '', false);
    add_option('ftm_additional_partner_info_field_id', '', '', false);
    add_option('ftm_affiliate_partner_field_id', '', '', false);
}

$teamleader_requests_sender = new FunkeyTeamleaderRequestsSender();

add_action('plugins_loaded', ['FunkeyTeamleaderAdminPage', 'initActions']);
add_action('plugins_loaded', ['FunkeyTeamleaderWooSettings', 'initActions']);
add_action('plugins_loaded', ['FunkeyTeamleaderOrdersActions', 'initActions']);
add_action('plugins_loaded', ['FunkeyTeamleaderHooks', 'initActions']);
add_action('plugins_loaded', function() {
    require_once __DIR__ . '/classes/NF_Actions_TeamLeader.php';
    add_filter('ninja_forms_register_actions', 'add_teamleader_nf_actions');
});
add_action('plugins_loaded', function () {
	$pl = new \Funkey\Main();
	$pl->run();
});

/**
 * Register Ninja Forms custom actions
 * @param array $actions
 * @return array mixed
 */
function add_teamleader_nf_actions($actions) {
    $actions['teamleader'] = new NF_Actions_TeamLeader;
    return $actions;
}

require_once plugin_dir_path( __FILE__ ) . 'autoloader.php';
$loader = new \Psr4AutoloaderClass();
$loader->register();
$loader->addNamespace( 'Funkey', __DIR__ . '/inc' );
