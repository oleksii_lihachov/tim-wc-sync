<?php if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Class NF_Actions_TeamLeader
 */
final class NF_Actions_TeamLeader extends NF_Abstracts_Action
{
    /**
     * @var string
     */
    protected $_name  = 'teamleader';

    /**
     * @var array
     */
    protected $_tags = array();

    /**
     * @var string
     */
    protected $_timing = 'normal';

    /**
     * @var int
     */
    protected $_priority = 10;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->_nicename = 'TeamLeader Integration';

        $this->_settings = [];
    }

    /*
    * PUBLIC METHODS
    */
    public function save($action_settings)
    {

    }

    public function process($action_settings, $form_id, $data)
    {
        $contact_info = [];
        $contact_keys = ['firstname', 'lastname', 'email'];

        foreach ($data['fields'] as $field) {
            if (in_array($field['type'], $contact_keys)) {
                $contact_info[$field['type']] = $field['value'];
            }
        }

        // exit if there is no email or last name
        if (!isset($contact_info['email']) || !isset($contact_info['lastname'])) {
            return $data;
        }
        if(!isset($contact_info['firstname'])) {
            $contact_info['firstname'] = '';
        }

        try {
            global $teamleader_requests_sender;
            $teamleader_requests_sender->createContact($contact_info['firstname'], $contact_info['lastname'], $contact_info['email']);
        } catch (Exception $e) {
            return $data;
        }

        return $data;
    }
}
