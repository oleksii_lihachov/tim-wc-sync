<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
// @codingStandardsIgnoreStart
class FunkeyTeamleaderClient {

    public $provider;
    public $token = [];

    public function __construct($clientId, $clientSecret, $token = null)
    {
        $redirectUri = get_admin_url() . 'options-general.php?page=teamleader-settings';

        $this->provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $clientId,    // The client ID assigned to you by the provider
            'clientSecret'            => $clientSecret ,   // The client password assigned to you by the provider
            'redirectUri'             => $redirectUri,
            'urlAuthorize'            => 'https://app.teamleader.eu/oauth2/authorize',
            'urlAccessToken'          => 'https://app.teamleader.eu/oauth2/access_token',
            'urlResourceOwnerDetails' => 'https://api.teamleader.eu/users.me'
        ]);

        if ($token) {
            $this->token = new \League\OAuth2\Client\Token\AccessToken($token);
        }
    }

    public function authorize($code)
    {
        try {

            // Try to get an access token using the authorization code grant.
            $accessToken = $this->provider->getAccessToken('authorization_code', [
                'code' => $code
            ]);
            $this->token = $accessToken;

            // save token to DB
            update_option('ftm_access_token', $accessToken->jsonSerialize());

            // Using the access token, we may look up details about the
            // resource owner.
            $resourceOwner = $this->provider->getResourceOwner($accessToken);
            $response = $resourceOwner->toArray();

            return '<div class="updated settings-error notice is-dismissible">
                        <p><strong>Authorization succeed!</strong></p>
                        <p>User: '.$response['data']['first_name'] . ' ' . $response['data']['last_name'] .'</p>
                        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                    </div>';

        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            return '<div class="error">
                <p>' . $e->getMessage() .'</p>
            </div>';
        }
    }

    public function updateToken()
    {
        $newAccessToken = $this->provider->getAccessToken('refresh_token', [
            'refresh_token' => $this->token->getRefreshToken()
        ]);
        $this->token = $newAccessToken;
        update_option('ftm_access_token', $newAccessToken->jsonSerialize());
    }


    public function cleanPhoneNumber($phone_number){
        $phone_number = preg_replace("/([^0-9|^+])/", '', $phone_number);
        return $phone_number;
    }

    public function getContactId($email)
    {
        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/contacts.list',
            $this->token,
            ['body' => json_encode(['filter' => [
                'email' => [
                    'type' => 'primary',
                    'email' => $email
                ]
            ]])]
        );

        $response = $this->provider->getParsedResponse($request);

        if (!count($response['data'])) {
            return null;
        }
        return $response['data'][0]['id'];
    }

    public function addContact($first_name, $last_name, $email, $phone = "")
    {

        $phone = $this->cleanPhoneNumber($phone);
        $body = [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'emails' => [[
                'type' => 'primary',
                'email' => $email
            ]]
        ];

        if ($phone){
            $body['telephones'] = [[
                 'type' => 'phone',
                 'number' => $phone
             ]];
        }

        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/contacts.add',
            $this->token,
            ['body' => json_encode($body)]
        );

        $response = $this->provider->getParsedResponse($request);

        return $response['data']['id'];
    }

    public function getContact( $id ) {
        $request = $this->provider->getAuthenticatedRequest(
			'GET',
			'https://api.teamleader.eu/contacts.info',
			$this->token,
			[ 'body' => json_encode( $id ) ]
		);

		$response = $this->provider->getParsedResponse( $request );

		if ( isset( $response['errors'] ) ) {
			return array(
				'success' => false,
				'errors'  => $response['errors'],
			);
		}

		return array(
			'success' => true,
			'data'    => $response['data'],
		);
    }

    public function updateContact($data)
    {
        if(isset($data['telephones'][0]['number'])){
            $phone = $data['telephones'][0]['number'];
            $data['telephones'][0]['number'] = $this->cleanPhoneNumber($phone);
        };

        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/contacts.update',
            $this->token,
            ['body' => json_encode($data)]
        );

        $response = $this->provider->getParsedResponse($request);

        return $response;
    }

    /**
     * @param $deal_body
     * @return mixed
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     * @throws Exception
     */
    public function addDeal($deal_body)
    {
        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/deals.create',
            $this->token,
            ['body' => json_encode($deal_body)]
        );

        $response = $this->provider->getParsedResponse($request);

        if (!isset($response['data']['id'])) {
            throw new \Exception('Deal hasn\'t created');
        }

        return $response['data']['id'];
    }

    public function getDeal($deal_id)
    {
        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/deals.info',
            $this->token,
            ['body' => json_encode(['id' => $deal_id])]
        );

        $response = $this->provider->getParsedResponse($request);

        return $response['data'];
    }

    public function updateDeal($data)
    {
        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/deals.update',
            $this->token,
            ['body' => json_encode($data)]
        );

        $response = $this->provider->getParsedResponse($request);

        return $response;
    }

    public function deleteDeal($deal_id)
    {
        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/deals.delete',
            $this->token,
            ['body' => json_encode(['id' => $deal_id])]
        );

        $response = $this->provider->getParsedResponse($request);

        return $response;
    }

    /**
     * @param $email
     * @return mixed
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function getUserByEmail($email)
    {
        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/users.list',
            $this->token,
            ['body' => json_encode(['filter'=> ['term' => $email]])]
        );

        $response = $this->provider->getParsedResponse($request);

        return $response['data'][0];
    }

    /**
     * @param $email
     * @return bool
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function checkUserExistsByEmail($email)
    {
        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/users.list',
            $this->token,
            ['body' => json_encode(['filter'=> ['term' => $email]])]
        );

        $response = $this->provider->getParsedResponse($request);

        return $response['meta']['matches'] ? true : false;
    }

    public function registerDealMovedWebHook()
    {
        $this->provider->getParsedResponse($this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/webhooks.unregister',
            $this->token,
            ['body' => json_encode(['url' => get_rest_url() . 'funkey/wc/deal-moved',
                'types' => ['deal.moved']])]
        ));

        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/webhooks.register',
            $this->token,
            ['body' => json_encode(['url' => get_rest_url() . 'funkey/wc/deal-moved',
                'types' => ['deal.moved']])]
        );

        $response = $this->provider->getParsedResponse($request);

        if (!$response) {
            return '<div class="updated settings-error notice is-dismissible">
                        <p><strong>Deal Moved Hook Registered!</strong></p>
                        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                    </div>';
        } else {
            return '<div class="error">
                <p>' . print_r($response['errors'], true) . '</p>
            </div>';
        }
    }

    public function registerDealUpdatedWebHook()
    {
        $this->provider->getParsedResponse($this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/webhooks.unregister',
            $this->token,
            ['body' => json_encode(['url' => get_rest_url() . 'funkey/wc/deal-updated',
                'types' => ['deal.updated']])]
        ));

        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/webhooks.register',
            $this->token,
            ['body' => json_encode(['url' => get_rest_url() . 'funkey/wc/deal-updated',
                'types' => ['deal.updated']])]
        );

        $response = $this->provider->getParsedResponse($request);

        if (!$response) {
            return '<div class="updated settings-error notice is-dismissible">
                        <p><strong>Deal Updated Hook Registered!</strong></p>
                        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                    </div>';
        } else {
            return '<div class="error">
                <p>' . print_r($response['errors'], true) . '</p>
            </div>';
        }
    }

    /**
     * @param $name
     * @param $vat_number
     * @param null | array $address
     * @return bool | array
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function createCompany($name, $vat_number, $address = null)
    {
        $body = compact('name', 'vat_number');

        if ($address && is_array($address)) {
            $body['addresses'] = [[
               'type' => 'primary',
                'address' => [
                    'line_1' => $address['street'] . ' ' . $address['number'] . ' ' . $address['box'],
                    'postal_code' => $address['zip_code'],
                    'city' => $address['city'],
                    'country' => $address['country']
                ]
            ]];
        }

        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/companies.add',
            $this->token,
            ['body' => json_encode($body)]
        );

        $response = $this->provider->getParsedResponse($request);

        if (isset($response['errors'])) {
            return false;
        }

        return $response['data']['id'];
    }

    /**
     * @param $vat_number
     * @return null | string
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function getCompanyIdByVat($vat_number)
    {
        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/companies.list',
            $this->token,
            ['body' => json_encode(['filter'=> ['term' => $vat_number]])]
        );

        $response = $this->provider->getParsedResponse($request);

        if (!count($response['data'])) {
            return null;
        }

        return $response['data'][0]['id'];
    }

    /**
     * @param $id
     * @param $company_id
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function linkContactToCompany($id, $company_id)
    {
        $request = $this->provider->getAuthenticatedRequest(
            'POST',
            'https://api.teamleader.eu/contacts.linkToCompany',
            $this->token,
            ['body' => json_encode(compact('id', 'company_id'))]
        );

        $this->provider->getParsedResponse($request);
    }

    public function getCustomFieldsList()
    {
        $page = [
            'size' => 100
        ];

        $request = $this->provider->getAuthenticatedRequest(
            'GET',
            'https://api.teamleader.eu/customFieldDefinitions.list',
            $this->token,
            ['body' => json_encode(compact('page'))]
        );

        $response = $this->provider->getParsedResponse($request);

        if (!count($response['data'])) {
            return null;
        }
        return $response['data'];
    }

    public function getDealPhasesList()
    {
        $page = [
            'size' => 100
        ];

        $request = $this->provider->getAuthenticatedRequest(
            'GET',
            'https://api.teamleader.eu/dealPhases.list',
            $this->token,
            ['body' => json_encode(compact('page'))]
        );

        $response = $this->provider->getParsedResponse($request);

        if (!count($response['data'])) {
            return null;
        }
        return $response['data'];
    }

	public function getProducts( $number = 1 ) {
         $page = [
            'size'      => 20,
            'number'    => $number,
        ];

		$request = $this->provider->getAuthenticatedRequest(
			'GET',
			'https://api.teamleader.eu/products.list',
			$this->token,
            ['body' => json_encode(compact('page'))]
		);

		$response = $this->provider->getParsedResponse( $request );

		if ( ! count( $response['data'] ) ) {
			return null;
		}
		return $response['data'];
	}

	public function getProductInfo( $id ) {
		$request = $this->provider->getAuthenticatedRequest(
			'GET',
			'https://api.teamleader.eu/products.info',
			$this->token,
			[ 'body' => json_encode( $id ) ]
		);

		$response = $this->provider->getParsedResponse( $request );

		if ( ! count( $response['data'] ) ) {
			return null;
		}
		return $response['data'];
	}

	public function addProduct( $body ) {
		$request = $this->provider->getAuthenticatedRequest(
			'POST',
			'https://api.teamleader.eu/products.add',
			$this->token,
			[ 'body' => json_encode( $body ) ]
		);

		$response = $this->provider->getParsedResponse( $request );

		if ( isset( $response['errors'] ) ) {
			return array(
				'success' => false,
				'errors'  => $response['errors'],
			);
		}

		return array(
			'success' => true,
			'data'    => $response['data'],
		);
	}

	public function addQuotation( $data ) {
		$request = $this->provider->getAuthenticatedRequest(
			'POST',
			'https://api.teamleader.eu/quotations.create',
			$this->token,
			[ 'body' => json_encode( $data ) ]
		);

		$response = $this->provider->getParsedResponse( $request );

		if ( ! isset( $response['data']['id'] ) ) {
			throw new \Exception( __('Quotation hasn\'t created', FT_TEXT_DOMAIN) );
		}

		return $response['data']['id'];
	}

    public function getQuotation( $id ) {
        $request = $this->provider->getAuthenticatedRequest(
			'GET',
			'https://api.teamleader.eu/quotations.info',
			$this->token,
			[ 'body' => json_encode( $id ) ]
		);

		$response = $this->provider->getParsedResponse( $request );

		if ( isset( $response['errors'] ) ) {
			return array(
				'success' => false,
				'errors'  => $response['errors'],
			);
		}

		return array(
			'success' => true,
			'data'    => $response['data'],
		);
    }

    public function updateQuotation( $body ) {
        $request = $this->provider->getAuthenticatedRequest(
			'POST',
			'https://api.teamleader.eu/quotations.update',
			$this->token,
			[ 'body' => json_encode( $body ) ]
		);

        $response = $this->provider->getParsedResponse( $request );
    
		return $this->provider->getParsedResponse( $request );
    }

	public function getTaxRates() {
		$request = $this->provider->getAuthenticatedRequest(
			'GET',
			'https://api.teamleader.eu/taxRates.list',
			$this->token
		);

		$response = $this->provider->getParsedResponse( $request );

		if ( ! count( $response['data'] ) ) {
			throw new \Exception( __('Failed to get Tax Rates', FT_TEXT_DOMAIN) );
		}

		return $response['data'];
	}
}
