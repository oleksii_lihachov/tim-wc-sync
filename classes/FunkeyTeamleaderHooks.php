<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

require_once FT_PLUGIN_DIR . '/inc/class-wcrest.php';

class FunkeyTeamleaderHooks {

    public static function initActions()
    {
        add_action('rest_api_init', [__CLASS__, 'registerWebHooksApi']);
    }

    public static function registerWebHooksApi()
    {
        register_rest_route( 'funkey/wc', '/deal-moved', array(
            'methods' => 'POST',
            'callback' => [__CLASS__, 'restDealMovedHandler'],
        ) );

        register_rest_route( 'funkey/wc', '/deal-updated', array(
            'methods' => 'POST',
            'callback' => [__CLASS__, 'restDealUpdatedHandler'],
        ) );
    }

    /**
     * @param  WP_REST_Request $request
     * @return bool
     */
    public static function restDealMovedHandler($request)
    {
        $req_body = json_decode($request->get_body(), true);

        if (!isset($req_body['type']) || ($req_body['type'] !== 'deal.moved')) {
            return false;
        }

        if (!isset($req_body['subject']['type']) || ($req_body['subject']['type'] !== 'deal')) {
            return false;
        }

        $deal_id = $req_body['subject']['id'];
        $new_order_status = '';
        $deal_phase_sent_id = get_option('ftm_deal_phase_sent');
        $deal_phase_won_id = get_option('ftm_deal_phase_won');
        $deal_phase_confirmed_id = get_option('ftm_deal_phase_confirmed');


        try {
            global $teamleader_requests_sender;
            $deal = $teamleader_requests_sender->getDeal($deal_id);

            if ($deal_phase_sent_id === $deal['current_phase']['id']) {
                $new_order_status = 'wc-quote-sent';
            }

            if ($deal_phase_confirmed_id === $deal['current_phase']['id']) {
                $new_order_status = 'wc-approv-to-vendor';
            }

            if ($deal_phase_won_id === $deal['current_phase']['id']) {
                $new_order_status = 'wc-quote-approved';
            }


            if ('lost' === $deal['status']) {
                $new_order_status = 'wc-quote-denied';
            }

            if ($new_order_status) {
                self::updateOrdersStatuses($new_order_status, $deal_id);
            }

            self::syncQoutes( $deal, $deal_id );

        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public static function syncQoutes( $deal, $deal_id ) {
		if ( ! $deal['quotations'] ) {
			return;
		}

        global $teamleader_requests_sender;

		$quotation_id   = $deal['quotations'][0]['id'];
		$quotation_data = $teamleader_requests_sender->getQuotation( $quotation_id );

		if ( false === $quotation_data['success'] && isset( $quotation_data['errors'] ) ) {
			return false;
		}

		$customer_id   = $deal['lead']['customer']['id'];
		$customer_info = $teamleader_requests_sender->getContact( $customer_id );

		if ( false === $customer_info['success'] && isset( $customer_info['errors'] ) ) {
			return false;
		}

		$tm_products_ids  = array();
		$tm_products_info = array();
		$grouped_lines    = $quotation_data['data']['grouped_lines'];

		foreach ( $grouped_lines as $grouped_line ) {
			foreach ( $grouped_line['line_items'] as $line ) {
				$tm_products_ids[] = $line['product']['id'];

				$tm_products_info[ $line['product']['id'] ] = array(
					'quantity' => $line['quantity'],
				);
			}
		}

		$query = new WP_Query(
			array(
				'posts_per_page' => -1,
        		'post_status'    => 'any',
				'post_type'      => 'shop_order',
				'meta_query'     => array(
					array(
						'key'   => 'teamleader_quote_id',
						'value' => $deal_id,
					),
				),
			)
		);

		if ( empty( $query->posts ) ) {
			return false;
		}

		$wc_rest               = new \Funkey\WC_Rest();
		$manager               = null;
		$order_tm_products_ids = array();

		foreach ( $query->posts as $post ) {
			$order   = $wc_rest->get_order( $post->ID );
			$manager = get_field( 'account_manager', $post->ID )['ID'];

			if ( empty( $order->line_items ) ) {
				continue;
			}

			foreach ( $order->line_items as $line ) {
				foreach ( $line->meta_data as $meta ) {
					if ( $meta->key === '_tm_product_id' && ! empty( $meta->value ) ) {
						$order_tm_products_ids[] = $meta->value;
					}
				}
			}
		}

		$diff    = array_diff( $order_tm_products_ids, $tm_products_ids );
		$tm_diff = array_diff( $tm_products_ids, $order_tm_products_ids );

		// Check if product deleted from TeamLeader Quotation and update status them.
		if ( count( $diff ) > 0 ) {
			foreach ( $query->posts as $post ) {
			  $wc_rest->update_order( $post->ID, array( 'status' => 'quote-denied' ) );
			}
		}

			// Check if new product added in TeamLeader Quotation and create new Order.
		if ( count( $tm_diff ) > 0 ) {
			$new_order = $wc_rest->create_order( $deal_id, $tm_diff, $tm_products_info, $customer_info['data'] );

			// Update custom fields.
			$fields = self::mapOrderFields( $deal );
			self::updateOrdersFields( $fields, $deal_id );

			// Update manager.
			if ( ! $manager ) {
				$manager = get_option( 'wc_settings_funkey_tab_acc_manager' );
			}

			update_field( 'account_manager', $manager, $new_order->id );
		}
	}

    /**
     * @param  WP_REST_Request $request
     * @return bool
     */
    public static function restDealUpdatedHandler( $request ) {
        global $teamleader_requests_sender;

        $req_body = json_decode( $request->get_body(), true );

        if ( ! isset( $req_body['type'] ) || ( $req_body['type'] !== 'deal.updated' ) ) {
            return false;
        }

        if ( ! isset( $req_body['subject']['type'] ) || ( $req_body['subject']['type'] !== 'deal' ) ) {
            return false;
        }

        $deal_id = $req_body['subject']['id'];
      
        try {
            $deal   = $teamleader_requests_sender->getDeal($deal_id);
            $fields = self::mapOrderFields( $deal );
            self::updateOrdersFields(
                $fields,
                $deal_id
            );
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    protected static function mapOrderFields( $deal ) {
        $location_field_id                  = get_option('ftm_activity_location_field_id');
        $date_field_id                      = get_option('ftm_activity_date_field_id');
        $time_field_id                      = get_option('ftm_activity_time_field_id');
        $participants_field_id              = get_option('ftm_number_participants_field_id');
        $additional_partner_info_field_id   = get_option('ftm_additional_partner_info_field_id');

        $new_location       = '';
        $new_date           = '';
        $new_time           = '';
        $new_participants   = '';
        $new_partner_info   = '';

        foreach ( $deal['custom_fields'] as $field)  {
            switch ( $field['definition']['id'] ) {
                case $location_field_id :
                    $new_location = $field['value'];
                    break;
                case $date_field_id :
                    $new_date = $field['value'];
                    break;
                case $time_field_id :
                    $new_time = $field['value'];
                    break;
                case $participants_field_id :
                    $new_participants = $field['value'];
                    break;
                case $additional_partner_info_field_id :
                    $new_partner_info = $field['value'];
                    break;
            }
        }

        return compact( 'new_location', 'new_time', 'new_date', 'new_participants', 'new_partner_info' );
    }

    protected static function updateOrdersStatuses($new_status, $quote_id)
    {
        $orders = new WP_Query([
            'post_type' => 'shop_order',
            'posts_per_page' => -1,
            'meta_key'		=> 'teamleader_quote_id',
            'meta_value'	=> $quote_id,
            'fields' => 'ids'
        ]);

        foreach ($orders->posts as $order_id) {
            $order = wc_get_order($order_id);
            $order->update_status($new_status, 'Status changed to ' . $new_status . ' via Teamleader webhook - ', true);
            $order->save();
        }
    }

    protected static function updateOrdersFields($data, $quote_id)
    {
        $orders = new WP_Query([
            'post_type'      => 'shop_order',
            'posts_per_page' => -1,
            'meta_key'		 => 'teamleader_quote_id',
            'meta_value'	 => $quote_id,
            'fields'         => 'ids'
        ]);

        foreach ($orders->posts as $order_id) {
            update_field('funkey_start_date', $data['new_date'], $order_id);
            update_field('funkey_start_time', $data['new_time'], $order_id);
            update_field('funkey_location', $data['new_location'], $order_id);
            update_field('funkey_participants', $data['new_participants'], $order_id);
            update_field('funkey_additional_partner_info', $data['new_partner_info'], $order_id);
        }
    }
}
