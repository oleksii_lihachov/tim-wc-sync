<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

class FunkeyTeamleaderWooSettings {

    /**
     * Bootstraps the class and hooks required actions & filters.
     */
    public static function initActions()
    {
        // Register woocommerce settings hooks
        add_filter('woocommerce_settings_tabs_array', [__CLASS__, 'createFunkeySection'], 50);
        add_filter('woocommerce_settings_tabs_funkey_settings', [__CLASS__, 'createFunkeySettings']);
        add_action('woocommerce_update_options_funkey_settings', [__CLASS__, 'updateFunkeySettings']);

        add_filter('acf/validate_value/name=account_manager', [__CLASS__, 'validateOrderAccountManager'], 10, 4);
        add_action('acf/save_post', [__CLASS__, 'updateTLDealInfo'], 999, 1);
    }

    /**
     * Add a new settings tab to the WooCommerce settings tabs array.
     *
     * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
     * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
     */
    public static function createFunkeySection($settings_tabs)
    {
        $settings_tabs['funkey_settings'] = 'Funkey';
        return $settings_tabs;
    }

    /**
     * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
     *
     * @uses woocommerce_admin_fields()
     * @uses self::renderSettings()
     */
    public static function createFunkeySettings()
    {
        woocommerce_admin_fields(self::renderSettings());
    }

    /**
     * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
     *
     * @uses woocommerce_update_options()
     * @uses self::renderSettings()
     */
    public static function updateFunkeySettings()
    {
        $post_data = wp_unslash($_POST);
        try {
            $user = get_userdata($post_data['wc_settings_funkey_tab_acc_manager']);
            global $teamleader_requests_sender;
            $user_exists = $teamleader_requests_sender->checkTeamleaderUserExists($user->user_email);

            if ($user_exists) {
                woocommerce_update_options(self::renderSettings());
            } else {
                WC_Admin_Settings::add_error("In order to save this user as the default account manager, please create a user with the same email address in Teamleader ({$user->user_email})");
            }
        } catch (Exception $e) {
            WC_Admin_Settings::add_error($e->getMessage());
        }
    }

    /**
     * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
     *
     * @return array Array of settings for @see woocommerce_admin_fields() function.
     */
    public static function renderSettings()
    {
        // Get users by roles
        $users = get_users([
            'role__in' => ['administrator', 'shop_manager', 'account_manager'],
            'fields' => ['ID', 'display_name']
        ]);

        // Create options for dropdown
        $select_users = ['' => '---'];

        foreach ($users as $user) {
            $select_users[$user->ID] = $user->display_name;
        }

        $settings = array(
            'section_title' => array(
                'name'     => 'Funkey  Settings',
                'type'     => 'title',
                'desc'     => '',
                'id'       => 'wc_settings_funkey_tab_title'
            ),
            'default_account_manager' => array(
                'name' => 'Default account manager',
                'type' => 'select',
                'id'   => 'wc_settings_funkey_tab_acc_manager',
                'options' => $select_users
            ),
            'section_end' => array(
                'type' => 'sectionend',
                'id' => 'wc_settings_funkey_tab_section_end'
            )
        );

        return apply_filters( 'woocommerce_settings_funkey_settings', $settings );
    }

    /**
     * Validate account manager in order
     *
     * @param $valid
     * @param $value
     * @param $field
     * @param $input
     * @return string
     */
    public static function validateOrderAccountManager($valid, $value, $field, $input)
    {
        // Bail early if value is already invalid
        if(!$valid) {
            return $valid;
        }

        try {
            $user = get_userdata($value);
            global $teamleader_requests_sender;
            $user_exists = $teamleader_requests_sender->checkTeamleaderUserExists($user->user_email);

            if (!$user_exists) {
                $valid = "Please provide a user in Teamleader with the email address {$user->user_email} in order to save this user as the order account manager.";
            }
        } catch (Exception $e) {
            $valid = $e->getMessage();
        }

        return $valid;
    }

    public static function updateTLDealInfo($post_id)
    {
        if (get_post_type($post_id) !== 'shop_order') {
            return;
        }

        if (!get_field('teamleader_quote_id', $post_id)) {
            return;
        }

        $order = wc_get_order($post_id);
        $items = $order->get_items();
        $product_id = array_shift($items)->get_product_id();

        global $teamleader_requests_sender;
        $teamleader_requests_sender->updateDealInfoOnOrderUpdate($order, $product_id);
    }
}
