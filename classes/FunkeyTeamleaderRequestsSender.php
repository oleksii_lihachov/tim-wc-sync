<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
// @codingStandardsIgnoreStart
class FunkeyTeamleaderRequestsSender {

    private $teamleader_client;

    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'initActions']);
        $this->teamleader_client = $this->getTeamleaderClient();
    }

    public function initActions()
    {
        add_action('funkey_order_created', [$this, 'setDefaultAccManagerToOrder'], 10, 1);
    }

    /**
     * @return bool|FunkeyTeamleaderClient
     */
    protected function getTeamleaderClient()
    {
        try {
            $clientId = get_option('ftm_client_id');
            $clientSecret = get_option('ftm_client_secret');
            $token = get_option('ftm_access_token');

            if ($clientId && $clientSecret && $token) {
                $teamleader_client = new FunkeyTeamleaderClient($clientId, $clientSecret, $token);

                if ($teamleader_client->token->hasExpired()) {
                    $teamleader_client->updateToken();
                }

                return $teamleader_client;
            }

            return false;
        } catch (Exception $e) {
            error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/../funkey-teamleader.log');
            return false;
        }
    }

    public function setDefaultAccManagerToOrder($order_id)
    {
        // set default acc manager to new order
        $default_account_manager = get_option('wc_settings_funkey_tab_acc_manager');
        if ($default_account_manager)
            update_field('account_manager', $default_account_manager, $order_id);
    }

    /**
     * @param WC_Order $order
     * @param $product_id
     * @return boolean
     */
    public function createDeal($order, $product_id)
    {
        if(!$this->teamleader_client)
            return false;

        try {
            $product = wc_get_product($product_id);
            $deal_body = [];
            $first_name = $order->get_billing_first_name();
            $last_name = $order->get_billing_last_name();
            $email = $order->get_billing_email();
            $phone = $order->get_billing_phone();
            $start_date = get_field('funkey_start_date', $order->get_id());
            $company = $order->get_billing_company();
            $vat_number = get_field('company_vat_number', $order->get_id());
            $product_name = $product->get_name();
            $company_id = null;

            // get product discount field and check if it's on sale
            $discount = get_field('discount', $product_id);
            $product_is_on_sale = $product->is_on_sale();

            // get existing contact or create new one
            $contact_id = $this->teamleader_client->getContactId($order->get_billing_email());
            if (!$contact_id) {
                $contact_id = $this->teamleader_client->addContact(
                    $first_name,
                    $last_name,
                    $email,
                    $phone
                );
            }

            $body = [
                'id' => $contact_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'emails' => [[
                    'type' => 'primary',
                    'email' => $email
                ]],
                'custom_fields' => [
                    [
                        'id' => get_option('ftm_request_type_field_id'),
                        'value' => get_post_meta($order->get_id(), 'funkey_request_type', true),
                    ],
                ]
            ];

            if ($phone){
                $body['telephones'] = [[
                    'type' => 'phone',
                    'number' => $phone
                ]];
            }

            // set request type to contact
            $this->teamleader_client->updateContact($body);

            // get existing company or create new one if vat number is provided
            if ($vat_number && $company) {
                $company_id = $this->teamleader_client->getCompanyIdByVat($vat_number);
                if (!$company_id) {
                    $company_id = $this->teamleader_client->createCompany($company, $vat_number);
                }
            }

            // link a contact to a company
            if ($company_id) {
                $this->teamleader_client->linkContactToCompany($contact_id, $company_id);
            }

            // set the lead depending on if the company is exist
            if ($company_id) {
                $deal_body['lead'] = [
                    'customer' => [
                        'type' => 'company',
                        'id' => $company_id,
                    ],
                    'contact_person_id' => $contact_id,
                ];
            } else {
                $deal_body['lead']['customer'] = [
                    'type' => 'contact',
                    'id' => $contact_id
                ];
            }

            if ($company) {
                $client_display = $company . ' | ';
            } else {
                $client_display = $first_name . ' ' . $last_name . ' | ';
            }

            $deal_body['title'] = $client_display . $product_name . ' | ' . $start_date;

            $deal_body['estimated_value'] = [
                'amount' => $order->get_total(),
                'currency' => 'EUR'
            ];

            $participants = get_field('funkey_participants', $order->get_id());

            $deal_body['custom_fields'] = [
                [
                    'id' => get_option('ftm_activity_name_field_id'),
                    'value' => $product->get_title(),
                ],
                [
                    'id' => get_option('ftm_order_url_field_id'),
                    'value' => admin_url('post.php?post=' . $order->get_id() . '&action=edit'),
                ],
                [
                    'id' => get_option('ftm_catering_field_id'),
                    'value' => get_field('funkey_catering', $order->get_id()),
                ],
                [
                    'id' => get_option('ftm_order_id_field_id'),
                    'value' => strval($order->get_id()),
                ],
                [
                    'id' => get_option('ftm_attachment_field_id'),
                    'value' => get_field('quote_attachment', $product->get_id())['url'],
                ],
                [
                    'id' => get_option('ftm_number_participants_field_id'),
                    'value' => $participants ? $participants : 0,
                ],
                [
                    'id' => get_option('ftm_activity_location_field_id'),
                    'value' => get_field('funkey_location', $order->get_id()),
                ],
                [
                    'id' => get_option('ftm_activity_duration_field_id'),
                    'value' => get_field('duration', $product->get_id()),
                ],
                [
                    'id' => get_option('ftm_activity_date_field_id'),
                    'value' => get_field('funkey_start_date', $order->get_id()),
                ],
                [
                    'id' => get_option('ftm_activity_time_field_id'),
                    'value' => get_field('funkey_start_time', $order->get_id()),
                ],
                [
                    'id' => get_option('ftm_additional_partner_info_field_id'),
                    'value' => get_field('funkey_additional_partner_info', $order->get_id()),
                ],
            ];

            if ($affiliate = get_field('affiliate_partner', $order->get_id())) {
                $deal_body['custom_fields'][] = [
                    'id' => get_option('ftm_affiliate_partner_field_id'),
                    'value' => $affiliate->display_name
                ];
            }

            if ($product_is_on_sale && $discount) {
                $deal_body['custom_fields'][] = [
                    'id' => get_option('ftm_discount_field_id'),
                    'value' => $discount,
                ];
            }

            $account_manager = get_field('account_manager', $order->get_id());

            if ($account_manager) {
                $manager_teamleader = $this->teamleader_client->getUserByEmail($account_manager['user_email']);
                if ($manager_teamleader) {
                    $deal_body['responsible_user_id'] = $manager_teamleader['id'];
                }
            }

            $deal_id = $this->teamleader_client->addDeal($deal_body);
			update_field('teamleader_quote_id', $deal_id, $order->get_id());

            // Add order items TeamLeader IDs
            foreach ( $order->get_items() as $item_id => $item_obj ) {
                $tm_product_id = get_post_meta( $item_obj->get_product_id(), '_tm_product_id', true );
                wc_update_order_item_meta( $item_id, '_tm_product_id', $tm_product_id) ;
            }

			// Add price quote to TeamLeader Deal.
            $tax_rate_id = $this->getTaxId();

			if ( ! $tax_rate_id ) {
				throw new \Exception( __('Can`t fetch tax rate ID', FT_TEXT_DOMAIN) );
				return;
			}

			$body = array(
				'deal_id'       => $deal_id,
				'grouped_lines' => array(
					$this->buildGroupedLine( $order, $tax_rate_id ),
				),
			);

			$qoutation_id = $this->teamleader_client->addQuotation( $body );

			if ( $qoutation_id ) {
				update_post_meta( $order->get_id(), 'tm_quotation_id', $qoutation_id );
			}

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

	private function buildGroupedLine( $order, $tax_rate_id ) {
		$line = array(
			'section' 	=> array(
				'title' => 'Activiteiten'
			),
			'line_items' => array(),
		);

		foreach( $order->get_items() as $item ) {
			$item_product_id = $item->get_product_id();
			$line['line_items'][] = $this->buildLineItem( $item_product_id, $tax_rate_id );
		}

		return $line;
	}

	private function buildLineItem( $product_id, $tax_rate_id ) {
		$product 		= wc_get_product( $product_id );
		$name 			= $product->get_name();
		$is_on_sale 	= $product->is_on_sale();
		$discount 		= get_field( 'discount', $product_id );
		$tm_product_id 	= get_post_meta( $product_id, '_tm_product_id', true );

		$line = array(
			'quantity' 			=> 1,
			'description' 		=> $name,
			'unit_price' 		=> array(
				'amount'   => 0,
				'currency' => 'EUR',
				'tax'      => 'excluding',
			),
			'tax_rate_id' 		=> $tax_rate_id,
			'purchase_price' 	=> array(
				'amount'   => 0,
				'currency' => 'EUR',
			),
		);

		if ( $is_on_sale && $discount ) {
			$line['discount'] = array(
				'value' => (int) $discount,
				'type'  => 'percentage',
			);
		}

		if ( ! empty( $tm_product_id ) ) {
			$line['product_id'] = $tm_product_id;
		}

		return $line;
	}

    private function getTaxId() {
        $taxes       = $this->teamleader_client->getTaxRates();
		$tax_rate_id = null;

	    foreach ( $taxes as $tax ) {
		    if ( $tax['rate'] == 0.21 ) {
                $tax_rate_id = $tax['id'];
                break;
			}
        }

		return $tax_rate_id;
    }

    public function checkTeamleaderUserExists($email)
    {
        if($this->teamleader_client) {
            return $this->teamleader_client->checkUserExistsByEmail($email);
        } else {
            return false;
        }
    }

    public function updateDealInfoOnOrderUpdate($order, $product_id)
    {
        try {
            $deal_id = get_field('teamleader_quote_id', $order->get_id());

            if ($deal_id) {
                $manager = get_field('account_manager', $order->get_id());
                $product = wc_get_product($product_id);

                // get product discount field and check if it's on sale
                $discount = get_field('discount', $product_id);
                $product_is_on_sale = $product->is_on_sale();

                $teamleader_user = $this->teamleader_client->getUserByEmail($manager['user_email']);

                $deal = $this->teamleader_client->getDeal($deal_id);

                $participants = get_field('funkey_participants', $order->get_id());

                // we need send all custom fields to prevent them disappear
                $update_body = [
                    'id' => $deal_id,
                    'lead' => [
                        'customer' => [
                            'type' => $deal['lead']['customer']['type'],
                            'id' => $deal['lead']['customer']['id']
                        ]
                    ],
                    'responsible_user_id' => $teamleader_user['id'],
                    'custom_fields' => [
                        [
                            'id' => get_option('ftm_activity_name_field_id'),
                            'value' => $product->get_title(),
                        ],
                        [
                            'id' => get_option('ftm_order_url_field_id'),
                            'value' => admin_url('post.php?post=' . $order->get_id() . '&action=edit'),
                        ],
                        [
                            'id' => get_option('ftm_catering_field_id'),
                            'value' => get_field('funkey_catering', $order->get_id()),
                        ],
                        [
                            'id' => get_option('ftm_order_id_field_id'),
                            'value' => strval($order->get_id()),
                        ],
                        [
                            'id' => get_option('ftm_attachment_field_id'),
                            'value' => get_field('quote_attachment', $product->get_id())['url'],
                        ],
                        [
                            'id' => get_option('ftm_number_participants_field_id'),
                            'value' => $participants ? $participants : 0,
                        ],
                        [
                            'id' => get_option('ftm_activity_location_field_id'),
                            'value' => get_field('funkey_location', $order->get_id()),
                        ],
                        [
                            'id' => get_option('ftm_activity_duration_field_id'),
                            'value' => get_field('duration', $product->get_id()),
                        ],
                        [
                            'id' => get_option('ftm_activity_date_field_id'),
                            'value' => get_field('funkey_start_date', $order->get_id()),
                        ],
                        [
                            'id' => get_option('ftm_activity_time_field_id'),
                            'value' => get_field('funkey_start_time', $order->get_id()),
                        ],
                        [
                            'id' => get_option('ftm_additional_partner_info_field_id'),
                            'value' => get_field('funkey_additional_partner_info', $order->get_id()),
                        ],
                    ]
                ];

                if ($affiliate = get_field('affiliate_partner', $order->get_id())) {
                    $update_body['custom_fields'][] = [
                        'id' => get_option('ftm_affiliate_partner_field_id'),
                        'value' => $affiliate->display_name
                    ];
                }

                if ($product_is_on_sale && $discount) {
                    $update_body['custom_fields'][] = [
                        'id' => get_option('ftm_discount_field_id'),
                        'value' => $discount,
                    ];
                }

                $this->teamleader_client->updateDeal($update_body);
            }
        } catch (Exception $e) {
            error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/../funkey-teamleader.log');
            return;
        }
    }

    public function mergeDealInfo(
        $deal_id,
        $base_order,
        $order_ids,
        $product_names,
        $product_attachment_url,
        $product_duration,
        $product_discount
    )
    {
        try {

            $deal = $this->teamleader_client->getDeal($deal_id);
            $participants = get_field('funkey_participants', $base_order);

            // we need to update all custom fields.
            $update_body = [
                'id' => $deal_id,
                'lead' => [
                    'customer' => [
                        'type' => $deal['lead']['customer']['type'],
                        'id' => $deal['lead']['customer']['id']
                    ]
                ],
                'custom_fields' => [
                    [
                        'id' => get_option('ftm_activity_name_field_id'),
                        'value' => implode(', ', $product_names),
                    ],
                    [
                        'id' => get_option('ftm_order_url_field_id'),
                        'value' => admin_url('post.php?post=' . $base_order . '&action=edit'),
                    ],
                    [
                        'id' => get_option('ftm_catering_field_id'),
                        'value' => get_field('funkey_catering', $base_order),
                    ],
                    [
                        'id' => get_option('ftm_order_id_field_id'),
                        'value' => implode(', ', $order_ids),
                    ],
                    [
                        'id' => get_option('ftm_attachment_field_id'),
                        'value' => $product_attachment_url,
                    ],
                    [
                        'id' => get_option('ftm_number_participants_field_id'),
                        'value' => $participants ? $participants : 0,
                    ],
                    [
                        'id' => get_option('ftm_activity_location_field_id'),
                        'value' => get_field('funkey_location', $base_order),
                    ],
                    [
                        'id' => get_option('ftm_activity_duration_field_id'),
                        'value' => $product_duration,
                    ],
                    [
                        'id' => get_option('ftm_activity_date_field_id'),
                        'value' => get_field('funkey_start_date', $base_order),
                    ],
                    [
                        'id' => get_option('ftm_activity_time_field_id'),
                        'value' => get_field('funkey_start_time', $base_order),
                    ],
                    [
                        'id' => get_option('ftm_additional_partner_info_field_id'),
                        'value' => get_field('funkey_additional_partner_info', $base_order),
                    ],
                ]
            ];

            if ($affiliate = get_field('affiliate_partner', $base_order)) {
                $update_body['custom_fields'][] = [
                    'id' => get_option('ftm_affiliate_partner_field_id'),
                    'value' => $affiliate->display_name
                ];
            }

            if ($product_discount) {
                $update_body['custom_fields'][] = [
                    'id' => get_option('ftm_discount_field_id'),
                    'value' => $product_discount,
                ];
            }

            $this->teamleader_client->updateDeal($update_body);

        } catch (Exception $e) {
            return;
        }
    }

    public function deleteDeals($deals_ids)
    {
        foreach ($deals_ids as $deal_id) {
            $this->teamleader_client->deleteDeal($deal_id);
        }
    }

    public function getDeal($deal_id)
    {
        return  $this->teamleader_client->getDeal($deal_id);
    }

    public function createContact($first_name, $last_name, $email)
    {
        return $this->teamleader_client->addContact($first_name, $last_name, $email);
    }

    public function getContact( $id ) {
          return $this->teamleader_client->getContact(
            array(
                'id' => $id,
            )
        );
    }

    public function getOrCreateContact($first_name, $last_name, $email)
    {
        try {
            // get existing contact
            $contact_id = $this->teamleader_client->getContactId($email);
            // create new one if not found
            if (!$contact_id) {
                $contact_id = $this->teamleader_client->addContact($first_name, $last_name, $email);
            }

            return $contact_id;
        } catch (\Exception $e) {
            error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/../funkey-teamleader.log');
            return false;
        }
    }

    public function getOrCreateCompany($name, $vat_number, $address = null)
    {
        try {
            $company_id = $this->teamleader_client->getCompanyIdByVat($vat_number);
            if (!$company_id) {
                $company_id = $this->teamleader_client->createCompany($name, $vat_number, $address);
            }
            return $company_id;
        } catch (\Exception $e) {
            error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/../funkey-teamleader.log');
            return false;
        }
    }

    public function linkContactToCompany($contact_id, $company_id)
    {
        try {
            $this->teamleader_client->linkContactToCompany($contact_id, $company_id);
        } catch (\Exception $e) {
            error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/../funkey-teamleader.log');
        }
    }

	public function getProducts( $page ) {
		return $this->teamleader_client->getProducts( $page );
	}

	public function getProductInfo( $id ) {
		return $this->teamleader_client->getProductInfo( $id );
	}

	public function addProduct( $body ) {
    	return $this->teamleader_client->addProduct( $body );
	}

    public function getQuotation( $id ) {
          return $this->teamleader_client->getQuotation(
            array(
                'id' => $id,
            )
        );
    }

	public function mergeQuotations( $qoutation_id, $base_order, $order_ids ) {
		$tax_rate_id = $this->getTaxId();
		
		if ( ! $tax_rate_id ) {
			throw new \Exception( __('Can`t fetch tax rate ID', FT_TEXT_DOMAIN) );
			return;
		}

		$body = array(
			'id' 			=> $qoutation_id,
			'grouped_lines' => array(),
		);

		foreach ( $order_ids as $order_id ) {
			$order = wc_get_order( $order_id );
			$body['grouped_lines'][] = $this->buildGroupedLine( $order, $tax_rate_id );
		}

		$response = $this->teamleader_client->updateQuotation( $body );
		
		if ( isset( $response['errors'] ) ) {
			return new WP_Error( '404', $response['errors'][0]['title'] );
		}

		return $response;
	}
}
