<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

class FunkeyTeamleaderAdminPage {

    private static $customFieldsLabels = [
        'ftm_activity_name_field_id' => 'Activity name custom field',
        'ftm_order_url_field_id' => 'Order URL custom field',
        'ftm_catering_field_id' => 'Catering custom field',
        'ftm_order_id_field_id' => 'WooCommerce Order ID field',
        'ftm_attachment_field_id' => 'Attachment URL field',
        'ftm_number_participants_field_id' => 'Number of participants field',
        'ftm_activity_duration_field_id' => 'Activity duration field',
        'ftm_activity_location_field_id' => 'Activity location field',
        'ftm_activity_date_field_id' => 'Activity date field',
        'ftm_activity_time_field_id' => 'Activity time field',
        'ftm_request_type_field_id' => 'Request type field',
        'ftm_discount_field_id' => 'Discount field',
        'ftm_additional_partner_info_field_id' => 'Additional information for the partner field',
        'ftm_affiliate_partner_field_id' => 'Affiliate partner field',
    ];

    public static function initActions()
    {
        add_action('admin_menu', [__CLASS__, 'createSettingsPage']);
        add_action('admin_init', [__CLASS__, 'registerSettings']);
    }


    public static function registerSettings()
    {
        register_setting('ftm_settings', 'ftm_client_id');
        register_setting('ftm_settings', 'ftm_client_secret');
        register_setting('ftm_settings', 'ftm_deal_phase_sent');
        register_setting('ftm_settings', 'ftm_deal_phase_won');
        register_setting('ftm_settings', 'ftm_deal_phase_confirmed');

        foreach (self::$customFieldsLabels as $field => $label) {
            register_setting('ftm_settings', $field);
        }
    }

    public static function createSettingsPage()
    {
        add_submenu_page(
            'options-general.php',
            'Teamleader Integration Settings',
            'Teamleader settings',
            'manage_options',
            'teamleader-settings',
            [__CLASS__, 'displaySettingsPage']
        );
    }

    public static function displaySettingsPage()
    { ?>
        <div class="wrap">

            <h1>Teamleader Integration Settings</h1>

            <?php $clientId = get_option('ftm_client_id');
            $clientSecret = get_option('ftm_client_secret');
            $token = get_option('ftm_access_token');
            $teamleaderClient = new FunkeyTeamleaderClient($clientId, $clientSecret, $token);
            $tm_custom_fields = $teamleaderClient->getCustomFieldsList();
            $tm_deal_phases = $teamleaderClient->getDealPhasesList();

            if (isset($_GET['code'])) {
                echo $teamleaderClient->authorize($_GET['code']);
            }

            if (isset($_GET['register_hooks']) && $token) {
                echo $teamleaderClient->registerDealMovedWebHook();
                echo $teamleaderClient->registerDealUpdatedWebHook();
            }
            ?>
            <form method="post" action="options.php" novalidate="novalidate">

                <h2>API</h2>

                <?php settings_fields( 'ftm_settings' ); ?>

                <table class="form-table">
                    <tbody>
                    <tr>
                        <th scope="row">
                            <label for="ftm_client_id">Client ID</label>
                        </th>
                        <td>
                            <input name="ftm_client_id" type="text" id="ftm_client_id" value="<?= $clientId; ?>" class="regular-text">
                        </td>
                    </tr>

                    <tr>
                        <th scope="row">
                            <label for="ftm_client_secret">Client Secret</label>
                        </th>
                        <td>
                            <input name="ftm_client_secret" type="text" id="ftm_client_secret" value="<?= $clientSecret; ?>" class="regular-text">
                        </td>
                    </tr>
                    </tbody>
                </table>

                <?php if ($clientId && $clientSecret) : ?>
                    <p>
                        <a class="button" href="<?= $teamleaderClient->provider->getAuthorizationUrl(); ?>">Authorize</a>
                    </p>
                <?php endif; ?>


                <?php if ($token && $teamleaderClient) : ?>
                    <p>
                        <a class="button"
                           href="<?= get_admin_url()?>options-general.php?page=teamleader-settings&register_hooks=true">
                            Register Webhooks
                        </a>
                    </p>

                <?php endif;?>

                <h2>DEALS</h2>

                <table class="form-table">
                    <tbody>


                    <?php if ($tm_deal_phases) :
                        $select_options = [];
                        foreach ($tm_deal_phases as $tm_deal_phase) {
                            $select_options[$tm_deal_phase['id']] = $tm_deal_phase['name'];
                        }

                        self::renderOptionSelect('ftm_deal_phase_sent', $select_options, 'Deal phase Quote Sent');
                        self::renderOptionSelect('ftm_deal_phase_won', $select_options, 'Deal Phase Price Quote Confirmed');
                        self::renderOptionSelect('ftm_deal_phase_confirmed', $select_options, 'Deal Phase Quote Confirmed to Partner');
                    endif; ?>

                    <?php if ($tm_custom_fields) :
                        $select_options = [];
                        foreach ($tm_custom_fields as $tm_custom_field) {
                            if ('sale' === $tm_custom_field['context'] || 'contact' === $tm_custom_field['context']) {
                                $select_options[$tm_custom_field['id']] = $tm_custom_field['label'];
                            }
                        }

                        foreach (self::$customFieldsLabels as $field => $label) {
                            self::renderOptionSelect($field, $select_options, $label);
                        }
                    endif; ?>

                    </tbody>
                </table>

				<h3><?php esc_html_e( 'Synchronise WooCommerce Products', FT_TEXT_DOMAIN ); ?></h3>
				<div id="sync_products_wrapper"></div>

                <?php submit_button(); ?>

            </form>
        </div>
    <?php }

    private static function renderOptionSelect($field, $options, $label)
    { ?>
        <tr>
            <th scope="row">
                <label for="<?= $field; ?>"><?= $label; ?></label>
            </th>
            <td>
                <select name="<?= $field; ?>" id="<?= $field; ?>" class="regular-text">
                    <option value="">----</option>
                    <?php $selected = get_option($field);
                    foreach ($options as $key => $value) : ?>
                        <option value="<?php echo esc_attr( $key ); ?>" <?php selected( $selected, $key, true ); ?>><?php echo esc_attr( $value ); ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    <?php }
}
