<?php
// @codingStandardsIgnoreStart
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

class FunkeyTeamleaderOrdersActions {
    private static $deny_statuses = array( 'quote-requested', 'approved', 'denied' );

    public static function initActions()
    {
        add_filter('bulk_actions-edit-shop_order', [__CLASS__, 'groupOrdersTLBulkAction'], 20, 1);
        add_filter('handle_bulk_actions-edit-shop_order', [__CLASS__, 'handleGroupOrdersTLAction'], 10, 3);

        add_action('admin_notices', [__CLASS__, 'renderAdminNotices']);
        add_action('add_meta_boxes', [__CLASS__, 'linkedOrdersMetaBox']);

        add_filter('woocommerce_order_actions', [__CLASS__, 'addCreateDealOrderAction']);
        add_action('woocommerce_order_action_create_deal', [__CLASS__, 'handleCreateDealOrderAction']);

        // cron tasks for retry to create deals in teamleader
        add_action('funkey_teamleader_retry_deal_creation', [__CLASS__, 'handleDealCreationCronJob'], 10, 2);
    }

    public static function groupOrdersTLBulkAction($actions)
    {
        $actions['group_orders_tl'] = 'Group orders under 1 price quote';
        return $actions;
    }

    public static function handleGroupOrdersTLAction($redirect_to, $action, $order_ids)
    {
        if ( $action !== 'group_orders_tl' )
            return $redirect_to; // Exit

        $redirect_to = remove_query_arg(['orders_merge_error', 'orders_merge_success']);

        $oldest_order = min($order_ids);

        // if there are different email return an error
        if (!self::isOrdersHaveSameEmail($order_ids)) {
            $redirect_to = add_query_arg(['orders_merge_error' => 'emails'], $redirect_to);
            return $redirect_to;
        }

        $quote_ids = [];
        $order_quote_rels = [];
        foreach ($order_ids as $order_id) {
            $quote_id = get_field('teamleader_quote_id', $order_id);
            if ($quote_id) {
                $quote_ids[] = $quote_id;
                $order_quote_rels[$order_id] = $quote_id;
            }
        }

        $quotes_counts = array_count_values($quote_ids);
        arsort($quotes_counts);


        try {
            // if no quotes found
            if (empty($quote_ids)) {
                // create deal by the oldest order
                self::createNewDealFromOldestOrder($oldest_order);
                self::setDealToOrders($oldest_order, $order_ids);
                self::setQuotationsToOrders( $oldest_order, $order_ids );
                self::updateDealWithMergedInfo($oldest_order, $order_ids);
            }

            // if all of the orders have different quotes
            if (count($order_ids) === count($quotes_counts)) {

                self::setDealToOrders($oldest_order, $order_ids);
                self::setQuotationsToOrders( $oldest_order, $order_ids );
                self::updateDealWithMergedInfo($oldest_order, $order_ids);
                // delete oldest order quote id from array
                unset($order_quote_rels[$oldest_order]);

                self::deleteRedunantDeals(array_values($order_quote_rels));
            }

            // if some orders don't have quotes or have the same quotes
            if (count($order_ids) !== count($quotes_counts)) {
                // get 'dominant' quote id
                $dominant_quote = array_shift(array_keys($quotes_counts));
                $dominant_order = array_search($dominant_quote, $order_quote_rels);

                // delete dominant quote from array of deals
                while (array_search($dominant_quote, $order_quote_rels)) {
                    unset($order_quote_rels[array_search($dominant_quote, $order_quote_rels)]);
                }

                self::setDealToOrders($dominant_order, $order_ids);
                self::setQuotationsToOrders( $dominant_order, $order_ids );
                self::updateDealWithMergedInfo($dominant_order, $order_ids);
                self::deleteRedunantDeals(array_values($order_quote_rels));
            }
        } catch (Exception $e) {
            wp_die($e->getMessage());
        }

        $redirect_to = add_query_arg(['orders_merge_success' => 'true'], $redirect_to);
        return $redirect_to;
    }

    private static function createNewDealFromOldestOrder($order_id)
    {
        $order = wc_get_order($order_id);
        $items = $order->get_items();
        $product_id = array_shift($items)->get_product_id();

        global $teamleader_requests_sender;
        $teamleader_requests_sender->createDeal($order, $product_id);
    }

    private static function setDealToOrders($base_order, $order_ids)
    {
        unset($order_ids[array_search($base_order, $order_ids)]);

        $manager = get_field('account_manager', $base_order)['ID'];
        if (!$manager)
            $manager = get_option('wc_settings_funkey_tab_acc_manager');

        $quote_id = get_field('teamleader_quote_id', $base_order);

        foreach ($order_ids as $order_id) {
            update_field('teamleader_quote_id', $quote_id, $order_id);
            update_field('account_manager', $manager, $order_id);
        }
    }

    private static function setQuotationsToOrders( $base_order, $order_ids ) {
        unset( $order_ids[ array_search( $base_order, $order_ids ) ] );
        $quotation_id = get_post_meta( $base_order, 'tm_quotation_id', true );
        
        foreach ( $order_ids as $id ) {
            update_post_meta( $id, 'tm_quotation_id', $quotation_id );
        }
    }

    private static function updateDealWithMergedInfo($base_order, $order_ids)
    {
        global $teamleader_requests_sender;

        $deal_id = get_field('teamleader_quote_id', $base_order);
        $product_names = [];
        $product_attachment_url = '';
        $product_duration = '';
        $product_discount = null;

        $main_order = wc_get_order( $base_order );
        $status = $main_order->get_status();
        $quotation_id = get_post_meta( $base_order, 'tm_quotation_id', true );

        foreach ($order_ids as $order_id) {
            $order = wc_get_order($order_id);
            $items = $order->get_items();
            $product_id = array_shift($items)->get_product_id();
            $product = wc_get_product($product_id);
            $product_names[] = $product->get_title();
            if ($order_id == $base_order) {
                $product_attachment_url = get_field('quote_attachment', $product->get_id())['url'];
                $product_duration = get_field('duration', $product->get_id());
                $product_discount = $product->is_on_sale() ? get_field('discount', $product->get_id()) : null;
            }
        }

        $teamleader_requests_sender->mergeDealInfo(
            $deal_id,
            $base_order,
            $order_ids,
            $product_names,
            $product_attachment_url,
            $product_duration,
            $product_discount
        );

        if ( $quotation_id && in_array( $status, self::$deny_statuses ) ) {
            $result = $teamleader_requests_sender->mergeQuotations( $quotation_id, $base_order, $order_ids );
            
            if ( is_wp_error( $result ) ) {
                throw new Error($result->get_error_message());
            }
        }
    }

    private static function deleteRedunantDeals($deals_ids)
    {
        global $teamleader_requests_sender;
        $teamleader_requests_sender->deleteDeals($deals_ids);
    }

    private static function isOrdersHaveSameEmail($order_ids)
    {
        $emails = [];
        foreach ($order_ids as $order_id) {
            $emails[] = wc_strtolower(get_post_meta($order_id, '_billing_email', true));
        }

        if (!count($emails)) {
            return false;
        }

        $unique_emails = array_unique($emails);

        return count($unique_emails) === 1;
    }

    private static function orderDeniedPriceQuote($current_order)
    {
        if (isset($_REQUEST['denied-quote'])) {
            $order_id = (int) $_REQUEST['denied-quote'];
            echo "<p>Order #$order_id has been successfully unlinked and denied</p>";
        }


        if (!isset($_REQUEST['deny-quote'])) {
            return false;
        }

        $order_id = (int) $_REQUEST['deny-quote'];

        // check if order exists
        if (!get_post_status($order_id)) {
            return false;
        }

        if (!get_field('teamleader_quote_id', $order_id)) {
            return false;
        }

        // empty Teamleader deal ID field
        update_field('teamleader_quote_id', '', $order_id);

        // update order status
        $order = wc_get_order($order_id);
        $order->update_status('wc-quote-denied', 'Status changed to through the "linked orders" metabox', true);
        $order->save();

        // Refresh page after order saving
        $redirect_url = admin_url('post.php?post=' . $current_order->ID . '&action=edit&denied-quote=' . $order_id . '#order-linked-orders');
        echo "<script>document.location.href = '$redirect_url';</script>";
    }

    public static function linkedOrdersMetaBox()
    {
        add_meta_box('order-linked-orders', 'Linked Orders', [__CLASS__, 'renderLinkedOrdersMetaBox'], 'shop_order', 'normal');
    }

    public static function addCreateDealOrderAction($actions)
    {
        $actions['create_deal'] = 'Create deal in Teamleader';
        return $actions;
    }

    /**
     * @param WC_Order $order
     */
    public static function handleCreateDealOrderAction($order)
    {
        $items = $order->get_items();
        $product_id = array_shift($items)->get_product_id();

        // delete teamleader quote id field from POST
        if(isset($_POST['acf']['field_5dcac543f895f']))
            unset($_POST['acf']['field_5dcac543f895f']);

        global $teamleader_requests_sender;
        $result = $teamleader_requests_sender->createDeal($order, $product_id);

        // If creation failed schedule the cron job
        if (!$result) {
            wp_schedule_single_event(time() + 900, 'funkey_teamleader_retry_deal_creation', [$order, $product_id]);
            wp_die(self::dealCreationFailedNotice());
        }
    }

    /**
     * @param WC_Order $order
     * @param $product_id
     */
    public static function handleDealCreationCronJob($order, $product_id)
    {
        $tm = new FunkeyTeamleaderRequestsSender();
        $result = $tm->createDeal($order, $product_id);

        // If creation failed send email to the admin
        if (!$result) {
            $to = get_option('admin_email');
            $subject = 'Teamleader Deal creation failed.';
            $message = 'Order ID: ' . $order->get_id() . PHP_EOL;
            $message .= 'Order link: ' . admin_url('post.php?post=' . $order->get_id() . '&action=edit');
            wp_mail($to, $subject, $message);
        }
    }

    public static function renderLinkedOrdersMetaBox($order)
    {
        self::orderDeniedPriceQuote($order);

        $deal_id = get_field('teamleader_quote_id', $order->ID);
        if (!$deal_id)
            return;

        $linked_orders = new WP_Query([
            'post_type'      => 'shop_order',
            'posts_per_page' => -1,
            'meta_key'		 => 'teamleader_quote_id',
            'meta_value'	 => $deal_id,
            'fields'         => 'ids'
        ]);

        if (!count($linked_orders->posts))
            return;

        $current_url = add_query_arg($_SERVER['QUERY_STRING'], '',  get_admin_url(null, '/post.php'));

        ?>
        <table width="100%">
            <thead align="left">
            <tr>
                <th>Activity name</th>
                <th>Activity Seller</th>
                <th>Start date</th>
                <th>Start time</th>
                <th>Catering</th>
                <th>Location</th>
                <th></th>
            </tr>
            </thead>
            <tbody align="left">
            <?php foreach ($linked_orders->posts as $order_id) :
                $linked_order_fields = get_fields($order_id);
                $linked_order = wc_get_order($order_id);
                $items = $linked_order->get_items();
                $product_id = array_shift($items)->get_product_id();
                $product = wc_get_product($product_id);
                $vendor = dokan_get_vendor_by_product($product);?>
                <tr>
                    <td>
                        <a href="<?= $linked_order->get_edit_order_url(); ?>">
                            <?= $product->get_title(); ?>
                            <?php if ($order_id == $order->ID)
                                echo ' (current)'; ?>
                        </a>
                    </td>
                    <td><?= $vendor->get_name(); ?></td>
                    <td><?= $linked_order_fields['funkey_start_date']; ?></td>
                    <td><?= $linked_order_fields['funkey_start_time']; ?></td>
                    <td><?= $linked_order_fields['funkey_catering'] ? 'Yes' : 'No'; ?></td>
                    <td><?= $linked_order_fields['funkey_location']; ?></td>
                    <td><a href="<?= $current_url; ?>&deny-quote=<?= $order_id; ?>" class="deny-quote button button-primary">Price quote denied</a></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <script>
            jQuery('.deny-quote').click(function (e) {
                if (!confirm('Please, confirm the action.')) {
                    e.preventDefault();
                }
            })
        </script>
        <?php
    }

    public static function renderAdminNotices()
    {
        if (isset($_REQUEST['orders_merge_error']) && $_REQUEST['orders_merge_error'] == 'emails') : ?>
            <div class="error notice">
                <p>For safety reasons you can't merge orders with a different email address. Please change the email address in the orders and try again.</p>
            </div>
        <?php endif;
        if (isset($_REQUEST['orders_merge_success']) && $_REQUEST['orders_merge_success'] == 'true') : ?>
            <div class="updated notice">
                <p>Orders were grouped successfully.</p>
            </div>
        <?php endif;
    }

    public static function dealCreationFailedNotice()
    {
        return '<p>Teamleader deal wasn\'t created. Cron job was scheduled.</p>';
    }
}
