<?php

namespace Funkey;

use function GuzzleHttp\json_decode;

class ProductSync {
	public function __construct() {
		register_post_meta(
			'product',
			'_tm_linked_products',
			array(
				'show_in_rest'  => true,
				'single'        => true,
				'type'          => 'string',
				'auth_callback' => function () {
					return current_user_can( 'edit_posts' );
				},
			)
		);

		register_post_meta(
			'product',
			'_tm_product_id',
			array(
				'show_in_rest'  => true,
				'single'        => true,
				'type'          => 'string',
				'auth_callback' => function () {
					return current_user_can( 'edit_posts' );
				},
			)
		);
	}

	public function register_meta_box() {
		add_meta_box(
			'product_sync_field',
			__( 'Linked Teamleader Products', FT_TEXT_DOMAIN ),
			array( $this, 'product_sync_html' ),
			'product',
			'normal',
			'default'
		);
	}

	public function product_sync_html() {
		global $post;

		$linked_products = get_post_meta( $post->ID, '_tm_linked_products', true );
		$data            = $linked_products;

		if ( empty( $linked_products ) ) {
			$data = json_encode(
				array(
					'default' => '',
					'items'   => array(),
				)
			);
		}

		if ( is_array( $linked_products ) ) {
			$data = json_encode(
				$linked_products,
				( JSON_UNESCAPED_UNICODE | JSON_HEX_AMP | JSON_HEX_QUOT )
			);
		}
		?>
		<div id="linked_products_wrapper"
			 data-post-id="<?php echo $post->ID; ?>"
			 data-initial-products="<?php echo esc_attr( $data ); ?>"></div>
		<?php
	}

	public function detect_product_creating( $new_status, $old_status, $post ) {
		global $post;

		if ( $post->post_type !== 'product' ) {
			return;
		}
		if ( 'publish' !== $new_status or 'publish' === $old_status ) {
			return;
		}

		$tm_products = Rest::get_all_products();
		if ( $tm_products['success'] ) {
			self::add_teamleader_product( $post->ID, $tm_products );
		}
	}

	public static function add_teamleader_product( $product_id, $tm_products ) {
		global $teamleader_requests_sender;

		$product          = wc_get_product( $product_id );
		$product_name     = $product->get_name();
		$linked_products  = get_post_meta( $product_id, '_tm_linked_products', true );
		$is_product_exist = false;
		$tm_product_id    = null;
		$tm_product       = null;

		foreach ( $tm_products['data']['result'] as $item ) {
			if ( strcmp( $product_name, $item['name'] ) === 0 ) {
				$is_product_exist = true;
				$tm_product_id    = $item['id'];
				$tm_product       = $item;
				break;
			}
		}

		if ( false === $is_product_exist ) {
			$name = $product_name;

			if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
				$name .= ' (' . strtoupper( ICL_LANGUAGE_CODE ) . ')';
			}

			$body = array(
				'name'           => $name,
				'purchase_price' => array(
					'amount'   => 0,
					'currency' => 'EUR',
				),
				'selling_price'  => array(
					'amount'   => 0,
					'currency' => 'EUR',
				),
			);

			$result = $teamleader_requests_sender->addProduct( $body );

			if ( false === $result['success'] ) {
				return false;
			}

			if ( $result['data']['id'] ) {
				$tm_product_id = $result['data']['id'];
				$tm_product    = $teamleader_requests_sender->getProductInfo( array( 'id' => $tm_product_id ) );
			}
		}

		if ( empty( $linked_products ) && $tm_product ) {
			update_post_meta(
				$product_id,
				'_tm_linked_products',
				json_encode(
					array(
						'default' => $tm_product_id,
						'items'   => array( $tm_product ),
					),
					( JSON_UNESCAPED_UNICODE | JSON_HEX_AMP | JSON_HEX_QUOT )
				)
			);
		} else {
			$linked_products_data = json_decode( $linked_products, true );
			if ( ! empty( $linked_products_data['items'] ) ) {
				$is_linked_item_exist = false;
				foreach ( $linked_products_data['items'] as $item ) {
					if ( $item['id'] === $tm_product_id ) {
						$is_linked_item_exist = true;
						break;
					}
				}

				if ( false === $is_linked_item_exist ) {
					$linked_products_data['items'][] = $tm_product;
				}

				update_post_meta(
					$product_id,
					'_tm_linked_products',
					json_encode(
						$linked_products_data,
						( JSON_UNESCAPED_UNICODE | JSON_HEX_AMP | JSON_HEX_QUOT )
					)
				);
			}
		}

		return update_post_meta( $product_id, '_tm_product_id', $tm_product_id );
	}

	public static function fetch_product_from_db( $id ) {
		global $wpdb;

		$query = "
		SELECT * 
		FROM {$wpdb->prefix}posts
		INNER JOIN {$wpdb->prefix}postmeta m1
			ON ( {$wpdb->prefix}posts.ID = m1.post_id )
		WHERE
			{$wpdb->prefix}posts.post_type = 'product'
			AND {$wpdb->prefix}posts.post_status = 'publish'
			AND 
				( m1.meta_key = '_tm_product_id' AND m1.meta_value = '{$id}' )
				OR		
				( m1.meta_key = '_tm_linked_products' AND LOCATE('{$id}', m1.meta_value) > 0 )
		";

		return $wpdb->get_results( $query, ARRAY_A );
	}

	public static function fetch_unlinked_products() {
		global $wpdb;

		$result = array();

		$query_product_id  = "
		SELECT *
		from {$wpdb->prefix}posts
		INNER JOIN {$wpdb->prefix}postmeta m1
			ON ( {$wpdb->prefix}posts.ID = m1.post_id )
		WHERE 
			(
				( m1.meta_key = '_tm_product_id' AND m1.meta_value = '' )
				OR
				( m1.meta_key = '_tm_product_id' AND m1.meta_value IS NULL )	
				OR 
				( m1.meta_key = '_tm_linked_products' AND m1.meta_value = '' )
				OR 
				( m1.meta_key = '_tm_linked_products' AND m1.meta_value IS NULL )
			)
			AND {$wpdb->prefix}posts.post_type = 'product'
			AND {$wpdb->prefix}posts.post_status = 'publish'
			
		";
		$result_product_id = $wpdb->get_results( $query_product_id, ARRAY_A );

		$query_linked_products  = "
		SELECT *
		from {$wpdb->prefix}posts as m2
		WHERE 
			m2.post_type = 'product'
			AND m2.post_status = 'publish'
			AND 
				NOT EXISTS (
					SELECT *
					from {$wpdb->prefix}postmeta as m3
					WHERE 
						m2.ID = m3.post_id
						AND 
							( m3.meta_key = '_tm_linked_products' )
				)
		";
		$result_linked_products = $wpdb->get_results( $query_linked_products, ARRAY_A );

		$result = array();

		if ( ! empty( $result_product_id ) ) {
			$result = $result_product_id;
		} elseif ( ! empty( $result_linked_products ) ) {
			return $result_linked_products;
		}

		if ( ! empty( $result_linked_products ) ) {
			foreach ( $result_linked_products as $item ) {
				$is_exist = false;

				foreach ( $result_product_id as $first_item ) {
					if ( $item['ID'] === $first_item['ID'] ) {
						$is_exist = true;
						break;
					}
				}

				if ( false === $is_exist ) {
					$result[] = $item;
				}
			}
		}

		return $result;
	}
}
