<?php

namespace Funkey;

use WP_REST_Server;
use WP_REST_Request;
use WP_Query;

class Rest {
	private $rest_namespace;

	public function __construct( $namespace ) {
		$this->rest_namespace = $namespace;
	}

	public function action_rest_api_init_trait() {
		register_rest_route(
			$this->rest_namespace,
			'/products-list/',
			array(
				'methods'             => WP_REST_Server::READABLE,
				'permission_callback' => function ( WP_REST_Request $request ) {
					return true;
				},
				'callback'            => array( $this, 'rest_products_list_handler' ),
			)
		);

		register_rest_route(
			$this->rest_namespace,
			'/update-linked-products/',
			array(
				'methods'             => WP_REST_Server::CREATABLE,
				'permission_callback' => function ( WP_REST_Request $request ) {
					return true;
				},
				'callback'            => array( $this, 'rest_update_linked_products_handler' ),
			)
		);

		register_rest_route(
			$this->rest_namespace,
			'/sync-products/',
			array(
				'methods'             => WP_REST_Server::READABLE,
				'permission_callback' => function ( WP_REST_Request $request ) {
					return true;
				},
				'callback'            => array( $this, 'rest_sync_products_handler' ),
			)
		);
	}

	public function rest_products_list_handler( WP_REST_Request $request ) {
		$is_filtered       = boolval( $request->get_params()['filtered'] );
		$post_id           = $request->get_params()['post_id'];
		$products          = self::get_all_products();
		$filtered_products = null;

		if ( false === $products['success'] ) {
			wp_send_json_error(
				array(
					'message' => $products['data']['error'],
				),
				400
			);
		}

		if ( empty( $products['data']['result'] ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'No TeamLeader products yes', FT_TEXT_DOMAIN ),
				),
				400
			);
		}

		if ( ! $is_filtered ) {
			wp_send_json_success( $products['data']['result'] );
		}

		$query = new WP_Query(
			array(
				'post_type'  => array( 'product' ),
				'meta_query' => array(
					array(
						'key'     => '_tm_linked_products',
						'value'   => array( '' ),
						'compare' => 'NOT IN',
					),
				),
			)
		);

		// Filter product that no yet connected
		if ( empty( $query->posts ) ) {
			$filtered_products = $products['data']['result'];
		} else {
			global $linked_products_ids;
			$linked_products_ids = array();

			foreach ( $query->posts as $post ) {
				$linked_products_info = get_post_meta( $post->ID, '_tm_linked_products', true );

				if ( is_array( $linked_products_info ) ) {
					$linked_products = $linked_products_info;
				} else {
					$linked_products = json_decode( $linked_products_info, true );
				}

				foreach ( $linked_products['items'] as $item ) {
					array_push( $linked_products_ids, $item['id'] );
				}
			}

			$filtered_products = array_values(
				array_filter(
					$products['data']['result'],
					function ( $product ) {
						global $linked_products_ids;
						return ! in_array( $product['id'], $linked_products_ids );
					}
				)
			);
		}

		// filter current product from list
		global $current_product_tm_id;
		if ( $current_product_tm_id = get_post_meta( $post_id, '_tm_product_id', true ) ) {
			$result = array_values(
				array_filter(
					$filtered_products,
					function ( $product ) {
						global $current_product_tm_id;
						return $product['id'] !== $current_product_tm_id;
					}
				)
			);
		} else {
			$result = $filtered_products;
		}

		wp_send_json_success( $result );
	}

	public function rest_update_linked_products_handler( WP_REST_Request $request ) {
		$body_json = $request->get_body();
		$body      = json_decode( $body_json, true );
		$result    = update_post_meta(
			$body['post_id'],
			'_tm_linked_products',
			json_encode(
				$body['data'],
				( JSON_UNESCAPED_UNICODE | JSON_HEX_AMP | JSON_HEX_QUOT )
			)
		);

		if ( ! $result ) {
			wp_send_json_error(
				array(
					'message' => 'Could not update post meta',
				),
				400
			);
		}

		wp_send_json_success( $result );
	}

	public static function get_all_products() {
		global $teamleader_requests_sender;

		$products    = array();
		$product_ids = array();
		$page        = 1;
		$is_process  = true;

		do {
			$tm_products = $teamleader_requests_sender->getProducts( $page );
			if ( $tm_products ) {
				foreach ( $tm_products as $item ) {
					$product_ids[] = $item;
				}
				$page++;
			} else {
				$is_process = false;
			}
		} while ( $is_process );

		if ( empty( $product_ids ) ) {
			return array(
				'success' => true,
				'data'    => array(
					'results' => array(),
				),
			);
		}

		foreach ( $product_ids as $id ) {
			$product_data = $teamleader_requests_sender->getProductInfo( $id );

			if ( $product_data ) {
				array_push( $products, $product_data );
			}
		}

		return array(
			'success' => true,
			'data'    => array(
				'result' => $products,
			),
		);
	}

	public function rest_sync_products_handler( WP_REST_Request $request ) {

		$unlinked_products = ProductSync::fetch_unlinked_products();

		if ( empty( $unlinked_products ) ) {
			wp_send_json_success( __( 'Not found unlinked WooCommerce products', FT_TEXT_DOMAIN ) );
		}

		$tm_products = self::get_all_products();

		if ( false === $tm_products['success'] ) {
			wp_send_json_error(
				array(
					'message' => __( 'Could not fetch TeamLeader products', FT_TEXT_DOMAIN ),
				),
				400
			);
		}

		$amount = 0;
		foreach ( $unlinked_products as $item ) {
			$result = ProductSync::add_teamleader_product( $item['ID'], $tm_products );

			if ( $result ) {
				$amount ++;
			}
		}

		wp_send_json_success( 'Created ' . $amount . ' products' );
	}
}
