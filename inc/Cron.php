<?php

namespace Funkey;

class Cron {
	public function __construct() {
		add_action( 'check_products_hook', array( $this, 'check_products' ) );
	}

	public function register_crons() {
		if ( ! wp_next_scheduled( 'check_products_hook' ) ) {
			wp_schedule_event( time(), 'daily', 'check_products_hook' );
		}
	}

	public function check_products() {
		global $teamleader_requests_sender;
		$products        = $teamleader_requests_sender->getProducts();
		$missed_products = array();

		if ( ! $products ) {
			return;
		}

		foreach ( $products as $product ) {
			$record = ProductSync::fetch_product_from_db( $product['id'] );

			if ( empty( $record ) ) {
				$missed_products[] = $product;
			}
		}

		if ( ! empty( $missed_products ) ) {
			$this->send_unlinked_products_email( $missed_products );

		}
	}

	private function send_unlinked_products_email( $products ) {
		$subject    = 'Belangrijk: niet-gekoppelde Teamleader artikelen';
		$manager_id = get_option( 'wc_settings_funkey_tab_acc_manager' );
		$user       = get_user_by( 'id', $manager_id );
		$to         = $user->user_email;

		$headers   = array();
		$headers[] = 'From: TeamLeader WP Plugin';
		$headers[] = 'content-type: text/html';

		if ( ! defined( 'WC_PLUGIN_FILE' ) ) {
			return false;
		}

		ob_start();

		include_once dirname( WC_PLUGIN_FILE ) . '/templates/emails/email-header.php';
		include FT_PLUGIN_DIR . '/templates/emails/unlinked-products-body.php';
		include_once dirname( WC_PLUGIN_FILE ) . '/templates/emails/email-footer.php';

		$message = ob_get_contents();
		ob_end_clean();

		return wp_mail( $to, $subject, $message, $headers );
	}
}
