<?php

namespace Funkey;

use Automattic\WooCommerce\Client;
use WP_Query;

class WC_Rest {
	private $woocommerce;
	public $error;

	public function __construct() {
		if ( ! defined( 'WC_CONSUMER_KEY' ) || ! defined( 'WC_CONSUMER_SECRET' ) ) {
			$this->error = 'Empty API keys';
			return;
		}

		$this->woocommerce = new Client(
			get_site_url(),
			WC_CONSUMER_KEY,
			WC_CONSUMER_SECRET,
			array(
				'wp_api'            => true,
				'version'           => 'wc/v3',
				'verify_ssl'        => false,
				'query_string_auth' => true,
			)
		);
	}

	public function get_order( $id ) {
		return $this->woocommerce->get( 'orders/' . $id );
	}

	public function update_order( $id, $data ) {
		$this->woocommerce->put( 'orders/' . $id, $data );
	}

	public function create_order( $tm_qoute_id, $tm_product_ids, $tm_products_info, $customer_info ) {
		$data = array(
			'meta_data'  => array(
				array(
					'key'   => 'teamleader_quote_id',
					'value' => $tm_qoute_id,
				),
			),
			'status'     => 'quote-sent',
			'line_items' => array(),
			'billing'    => array(
				'first_name' => $customer_info['first_name'],
				'last_name'  => $customer_info['last_name'],
				'email'      => $customer_info['emails'][0]['email'],
				'phone'      => $customer_info['telephones'][0]['number'],
			),
		);

		if ( ! empty( $customer_info['addresses'] ) ) {
			$data['billing']['address_1'] = $customer_info['addresses'][0]['address']['line_1'];
			$data['billing']['city']      = $customer_info['addresses'][0]['address']['city'];
			$data['billing']['postcode']  = $customer_info['addresses'][0]['address']['postal_code'];
			$data['billing']['country']   = $customer_info['addresses'][0]['address']['country'];
		}

		foreach ( $tm_product_ids as $tm_product_id ) {
			$query = new WP_Query(
				array(
					'post_status' => 'publish',
					'post_type'   => 'product',
					'meta_query'  => array(
						array(
							'key'   => '_tm_product_id',
							'value' => $tm_product_id,
						),
					),
				)
			);

			if ( empty( $query->posts ) ) {
				continue;
			}

			$data['line_items'][] = array(
				'product_id' => $query->posts[0]->ID,
				'quantity'   => $tm_products_info[ $tm_product_id ]['quantity'],
			);
		}

		return $this->woocommerce->post( 'orders', $data );
	}
}
