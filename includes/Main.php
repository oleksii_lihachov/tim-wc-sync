<?php

namespace Funkey;

class Main {
	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 */
	protected $version;

	private $rest_namespace = 'funkey/wc';

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 */
	public function __construct() {
		if ( defined( 'FT_VERSION' ) ) {
			$this->version = FT_VERSION;
		} else {
			$this->version = '1.0';
		}

		$this->plugin_name = FT_PLUGIN_SLUG;
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	private function load_dependencies() {
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Admin.php';

		$this->loader = new Loader();

		$product_sync = new ProductSync();
		$this->loader->add_action( 'add_meta_boxes', $product_sync, 'register_meta_box', 10, 0 );
		$this->loader->add_action( 'transition_post_status', $product_sync, 'detect_product_creating', 10, 3 );

		$rest = new Rest( $this->rest_namespace );
		$this->loader->add_action( 'rest_api_init', $rest, 'action_rest_api_init_trait', 10, 0 );

		$cron = new Cron();
		$cron->register_crons();
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the spsr_Landing_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 */
	private function set_locale() {
		$plugin_i18n = new i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 */
	private function define_admin_hooks() {
		$plugin_admin = new Admin( $this->plugin_name, $this->version, $this->rest_namespace );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 */
	private function define_public_hooks() {
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 */
	public function run() {
		$this->loader->run();
	}
}