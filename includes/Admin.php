<?php

namespace Funkey;

class Admin {
	private $plugin_name;
	private $version;
	private $rest_namespace;

	public function __construct( $plugin_name, $version, $rest_namespace ) {
		$this->plugin_name    = $plugin_name;
		$this->version        = $version;
		$this->rest_namespace = $rest_namespace;
	}

	public function enqueue_scripts() {
		wp_enqueue_script(
			$this->plugin_name . '-script',
			plugin_dir_url( __DIR__ ) . 'build/index.js',
			array( 'jquery', 'wp-blocks', 'wp-editor', 'wp-element', 'wp-i18n' ),
			$this->version,
			true
		);

		wp_localize_script(
			$this->plugin_name . '-script',
			'ft_object',
			array(
				'ajax_url'       => admin_url( 'admin-ajax.php' ),
				'plugin_path'    => plugin_dir_url( __DIR__ ),
				'rest_namespace' => $this->rest_namespace,
				'site_url'       => get_site_url(),
			)
		);

		wp_enqueue_style(
			$this->plugin_name,
			plugin_dir_url( __DIR__ ) . 'build/style-index.css'
		);
	}
}