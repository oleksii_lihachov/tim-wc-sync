 <p
     style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#101F32;font-size:16px">
     Eén of meerdere artikelen in Teamleader werden nog niet gekoppeld met een WooCommerce product. Dit zorgt ervoor dat
     partners niet op de hoogte worden gebracht bij het boeken van de activiteit.</p>
 <div style="padding:20px;Margin:0;font-size:0">
     <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation"
         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
         <tr>
             <td
                 style="padding:0;Margin:0;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px">
             </td>
         </tr>
     </table>
 </div>
 <p
     style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#101F32;font-size:16px">
     Het gaat om de volgende producten:<br></p>
 <ul>
     <?php
	 foreach ( $products as $product ) {
			?>
     <li
         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;Margin-bottom:15px;color:#101F32;font-size:16px">
         <?php echo $product['name']; ?>
     </li>
     <?php
	 }
		?>
 </ul>
 <div style="padding:20px;Margin:0;font-size:0">
     <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation"
         style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
         <tr>
             <td
                 style="padding:0;Margin:0;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px">
             </td>
         </tr>
     </table>
 </div>
 <p
     style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#101F32;font-size:16px">
     Producten linken doe je zo:</p>
 <ul>
     <li
         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;Margin-bottom:15px;color:#101F32;font-size:16px">
         Ga naar het product dat je wil linken in
         WooCommerce</li>
     <li
         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;Margin-bottom:15px;color:#101F32;font-size:16px">
         Ga naar 'Linked Teamleader products'</li>
     <li
         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;Margin-bottom:15px;color:#101F32;font-size:16px">
         Type hier de naam in van het product in
         Teamleader en klik erop</li>
     <li
         style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;Margin-bottom:15px;color:#101F32;font-size:16px">
         Klik op de blauwe knop 'Update'
         rechtsbovenaan.</li>
 </ul>