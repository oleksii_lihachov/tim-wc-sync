import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    margin: '20px 0'
  },
  loader: {
    textAlign: 'center',
    width: '100%',
    padding: '15px 0'
  },
  table: {
    width: '100%',
    marginBottom: '30px'
  },
  buttons: {
    '& > *': {
      marginLeft: 8,
      marginRight: 8,
    },
  },
  select: {
    '& input': {
      minHeight: 0,
      border: 'none'
    },
    '& input:focus': {
      border: 'none',
      boxShadow: 'none'
    }
  },
  createBtn: {
    width: '100%'
  },
  alert: {
    margin: '20px 0'
  }
}));

export default useStyles;