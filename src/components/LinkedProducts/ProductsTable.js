import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Button from "@material-ui/core/Button";
import TableContainer from "@material-ui/core/TableContainer";
import Table from '@material-ui/core/Table';

const ProductsTable = ({ classes, currentProducts, setDefault, removeItem }) => {
  return (
    <TableContainer>
      <Table className={classes.table} aria-label="table">
        <TableBody>
          {currentProducts.items.map((product) => {
            const isDefault = product.id === currentProducts.default;

            return (
              <TableRow key={product.id}>
                <TableCell component="th" scope="row">
                  {`${product.name}`} {isDefault ? '(Default)' : ''}
                </TableCell>
                <TableCell>
                  {`${product.selling_price.amount} ${product.selling_price.currency}`}
                </TableCell>
                <TableCell align="right">
                  <div className={classes.buttons}>
                    {!isDefault && (
                      <Button variant="outlined" onClick={() => setDefault(product.id)}>
                        Make default
                      </Button>
                    )}
                    <Button variant="outlined" color="secondary" onClick={() => removeItem(product.id)}>
                      Disconnect
                    </Button>
                  </div>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ProductsTable;