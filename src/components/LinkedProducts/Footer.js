import Grid from "@material-ui/core/Grid";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Checkbox from "@material-ui/core/Checkbox";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";

const icon = <CheckBoxOutlineBlankIcon fontSize="small"/>;
const checkedIcon = <CheckBoxIcon fontSize="small"/>;
const { Fragment } = wp.element;

const Footer = ({ products, handleChange, handleClick, classes }) => {
  return (
    <Grid container spacing={2} className={classes.select}>
      <Grid item xs={8}>
        <Autocomplete
          options={products}
          size="small"
          getOptionLabel={(option) => option.name}
          onChange={handleChange}
          renderOption={(option, { selected }) => (
            <Fragment>
              <Checkbox
                icon={icon}
                checkedIcon={checkedIcon}
                style={{ marginRight: 8 }}
                checked={selected}
              />
              {option.name}
            </Fragment>
          )}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" label="Products" placeholder=""/>
          )}/>
      </Grid>
      <Grid item xs={4}>
        <Button variant="contained" color="primary" className={classes.createBtn} onClick={handleClick}>
          Create
        </Button>
      </Grid>
    </Grid>
  );
};

export default Footer;