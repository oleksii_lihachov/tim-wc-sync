const { useEffect, useState, Fragment } = wp.element;
import { cloneDeep } from 'lodash';

import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';
import { ThemeProvider } from '@material-ui/styles';
import Alert from '@material-ui/lab/Alert';

import theme from "../../theme";
import useStyles from "./styles";
import ProductsTable from './ProductsTable';
import Footer from './Footer';
import ConfirmationAlert from './ConfirmationAlert';
import { API_BASE } from '../../constants';

const LinkedProducts = ({ initialProducts, postId }) => {
  const classes = useStyles();

  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [products, setProducts] = useState(null);
  const [currentProducts, setCurrentProducts] = useState(initialProducts);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [openRemovalDialog, setOpenRemovalDialog] = useState(false);
  const [openDefaultDialog, setOpenDefaultDialog] = useState(false);
  const [currentId, setCurrentId] = useState(null);

  // Load products (run just once)
  useEffect(() => {
    if (products) {
      return;
    }

    fetch(
      `${API_BASE}products-list/?filtered=1&&post_id=${postId}`,
      {
        method: 'GET'
      }
    )
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);

        if (!result.success) {
          setError(result.data.message);
          return;
        }

        setError(null);
        setProducts(result.data)
      })
      .catch(e => {
        setIsLoading(false);
        setError(e.message);
      });
  }, []);

  const handleChange = (ev, value) => {
    if (value) {
      setSelectedProduct(value);
    }
  };

  const updatePostMeta = (data) => {
    setIsLoading(true);

    fetch(
      `${API_BASE}update-linked-products/`,
      {
        method: 'POST',
        body: JSON.stringify({
          post_id: postId,
          data: data
        })
      })
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);

        if (result.success) {
          setCurrentProducts(data);
        }
      })
      .catch(e => {
        setIsLoading(false);
        setError(e.message);
        console.error(e);
      });
  };

  const handleClick = () => {
    if (!selectedProduct) {
      return;
    }

    // Prevent duplicate
    let isPresent = false;
    if (currentProducts && currentProducts.items.length > 0) {
      isPresent = currentProducts.items.some((item) => {
        return item.id === selectedProduct.id;
      });
    }

    if (isPresent) {
      return;
    }

    const clone = currentProducts ? cloneDeep(currentProducts) : {
      default: '',
      items: []
    };

    // Set first item default
    if (!clone.items.length) {
      clone.default = selectedProduct.id;
    }
    clone.items.push(selectedProduct);
    updatePostMeta(clone);
  };

  const handleOpenRemovalDialog = (id) => {
    setCurrentId(id);
    setOpenRemovalDialog(true);
  };

  const handleOpenDefaultDialog = (id) => {
    setCurrentId(id);
    setOpenDefaultDialog(true);
  };

  const handleCloseDialog = () => {
    setCurrentId(null);
    setOpenRemovalDialog(false);
    setOpenDefaultDialog(false);
  };

  const handleConfirmRemoval = () => {
    handleCloseDialog();

    const clone = cloneDeep(currentProducts);
    const index = clone.items.findIndex((item) => {
      return currentId === item.id
    });

    if (index !== -1) {
      clone.items.splice(index, 1);

      clone.items.length === 0 ? updatePostMeta(null) : updatePostMeta(clone);
    }
  };

  const handleConfirmDefault = () => {
    handleCloseDialog();
    const clone = cloneDeep(currentProducts);
    clone.default = currentId;
    updatePostMeta(clone);
  };


  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="md" className={classes.container}>
        <Grid container className={classes.root}>
          {error && (
            <Alert severity="error" className={classes.alert}>{error}</Alert>
          )}
          {!error && (
            <Fragment>
              {currentProducts && currentProducts.items.length > 0 && (
                <Grid item xs={12}>
                  <ProductsTable
                    currentProducts={currentProducts}
                    classes={classes}
                    setDefault={handleOpenDefaultDialog}
                    removeItem={handleOpenRemovalDialog}
                  />
                </Grid>
              )}
              {(products && products.length > 0) && (
                <Footer
                  products={products}
                  handleChange={handleChange}
                  classes={classes}
                  handleClick={handleClick}
                />
              )}
            </Fragment>
          )}
          {isLoading && (
            <div className={classes.loader}>
              <CircularProgress/>
            </div>
          )}
        </Grid>
        <ConfirmationAlert
          text="Are you sure you want to disconnect this product?"
          open={openRemovalDialog}
          handleClose={handleCloseDialog}
          handleAccept={handleConfirmRemoval}
        />
        <ConfirmationAlert
          text="Are you sure you want to make this product the default?"
          open={openDefaultDialog}
          handleClose={handleCloseDialog}
          handleAccept={handleConfirmDefault}
        />
      </Container>
    </ThemeProvider>
  );
};

export default LinkedProducts;