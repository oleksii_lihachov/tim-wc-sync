import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "@material-ui/core/Button";

const ConfirmationAlert = ({ text, open, handleClose, handleAccept }) => {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle>{text}</DialogTitle>
      <DialogActions>
        <Button onClick={handleAccept} color="primary">Yes</Button>
        <Button onClick={handleClose} color="secondary" autoFocus>No</Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmationAlert;