const { useEffect, useState, Fragment } = wp.element;

import Button from "@material-ui/core/Button";
import { ThemeProvider } from '@material-ui/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from '@material-ui/lab/Alert';

import theme from "../../theme";
import { API_BASE } from "../../constants";

const SyncProductsBox = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [syncResult, setSyncResult] = useState(null);

  const handleSync = () => {
    setIsLoading(true);
    fetch(
      `${API_BASE}sync-products/`,
      {
        method: 'GET'
      })
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);

        if (!result.success) {
          throw new Error(result.data.message)
        }

        setError(null);
        setSyncResult(result.data);
      })
      .catch(e => {
        setIsLoading(false);
        setError(e.message);
        console.error(e);
      });
  };

  return (
    <ThemeProvider theme={theme}>
      <table className="form-table">
        <tr>
          <th>
            <Button variant="outlined" onClick={handleSync}>
              Synchronise
            </Button>
          </th>
          <td>
            {isLoading && (
              <CircularProgress/>
            )}
            {error && (
              <Alert severity="error">{error}</Alert>
            )}
            {(!error && syncResult) && (
              <p>{syncResult}</p>
            )}
          </td>
        </tr>
      </table>
    </ThemeProvider>
  )
};

export default SyncProductsBox;
