import './style.scss';

const { render } = wp.element;
import LinkedProducts from "./components/LinkedProducts";
import SyncProductsBox from "./components/SyncProductsBox";

const box = document.getElementById(`linked_products_wrapper`);
const syncWrapper = document.getElementById(`sync_products_wrapper`);

if (box) {
  render(<LinkedProducts initialProducts={JSON.parse(box.dataset.initialProducts)} postId={box.dataset.postId}/>, box);
}

if (syncWrapper) {
  render(<SyncProductsBox/>, syncWrapper)
}