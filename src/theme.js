import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2271b1',
      dark: '#135e96'
    },
    secondary: {
      main: '#b32d2e',
      dark: '#b32d2e'
    },
  },
  overrides: {
    // Style sheet name ⚛️
    MuiButton: {
      // Name of the rule
      label: {
        // Some CSS
        textTransform: 'none',
      },

      outlined: {
        border: '1px solid #2271b1',
        color: '#2271b1'
      }
    },
  },
});

export default theme;